module Day02Spec where

import Test.Hspec

import Day02

spec :: Spec
spec = do
  let example1 = ["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"]
  describe "day02a" $ do
    it "counts two times correctly" $ do
      countTwos "abbacd" `shouldBe` 2
      countTwos "abbbcc" `shouldBe` 1
    it "counts three times correctly" $ do
      countThrees "abbacd" `shouldBe` 0
      countThrees "abbbcc" `shouldBe` 1
      countThrees "bbbccc" `shouldBe` 2
    it "example 1" $ do
      partA example1 `shouldBe` 12
  describe "day02b" $ do
    let example1 = ["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]
    it "difference" $ do
      difference "abc" "abc" `shouldBe` 0
      difference "abc" "adc" `shouldBe` 1
      difference "abo" "adc" `shouldBe` 2
      difference "asd" "qwe" `shouldBe` 3
    it "common" $ do
      common "abc" "abc" `shouldBe` "abc"
      common "abc" "adc" `shouldBe` "ac"
      common "asd" "aqw" `shouldBe` "a"
      common "qwe" "asd" `shouldBe` ""
    it "example 1" $ do
      partB example1 `shouldBe` "fgij"
