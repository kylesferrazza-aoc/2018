module LibSpec (spec) where

import Test.Hspec

import Lib

spec :: Spec
spec = do
  describe "readlines" $ do
    it "reads in0 as strings" $ do
      r <- readLines "in/0"
      let expected = ["1", "-2", "3", "+4", "5"]
      r `shouldBe` expected
  describe "removeLeadingPlus" $ do
    it "+12" $ do
      removeLeadingPlus "+12" `shouldBe` "12"
  describe "readInts" $ do
    it "reads in0 as ints" $ do
      r <- readInts "in/0"
      let expected = [1, -2, 3, 4, 5]
      r `shouldBe` expected
