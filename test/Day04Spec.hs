module Day04Spec where

import Test.Hspec

import Day04

import Data.Maybe(catMaybes)

spec :: Spec
spec = do
  let example1 = ["[1518-11-01 00:00] Guard #10 begins shift",
                  "[1518-11-01 00:05] falls asleep",
                  "[1518-11-01 00:25] wakes up",
                  "[1518-11-01 00:30] falls asleep",
                  "[1518-11-01 00:55] wakes up",
                  "[1518-11-01 23:58] Guard #99 begins shift",
                  "[1518-11-02 00:40] falls asleep",
                  "[1518-11-02 00:50] wakes up",
                  "[1518-11-03 00:05] Guard #10 begins shift",
                  "[1518-11-03 00:24] falls asleep",
                  "[1518-11-03 00:29] wakes up",
                  "[1518-11-04 00:02] Guard #99 begins shift",
                  "[1518-11-04 00:36] falls asleep",
                  "[1518-11-04 00:46] wakes up",
                  "[1518-11-05 00:03] Guard #99 begins shift",
                  "[1518-11-05 00:45] falls asleep",
                  "[1518-11-05 00:55] wakes up"]
      log = catMaybes $ map parseLogEntry example1 :: Log
  describe "parse log entries" $ do
    it "parsed all entries" $ do
      length log `shouldBe` length example1
    it "begin" $ do
      (log !! 0)
        `shouldBe` LogEntry (Time 1518 11 1 0 0) (Begin 10)
    it "asleep" $ do
      (log !! 1)
        `shouldBe` LogEntry (Time 1518 11 1 0 5) Asleep
    it "awake" $ do
      (log !! 2)
        `shouldBe` LogEntry (Time 1518 11 1 0 25) Awake
  -- describe "minutes asleep" $ do
  --   it "guard #10" $ do
  --     minutesAsleep log 10 `shouldBe` 50
  --   it "guard #99" $ do
  --     minutesAsleep log 99 `shouldBe` 30
  -- describe "part a" $ do
  --   it "example1" $ do
  --     partA example1 `shouldBe` 240
