module Day03Spec where

import Test.Hspec

import Day03

spec :: Spec
spec = do
  let rect1 = "#1 @ 1,3: 4x4"
      rect2 = "#2 @ 3,1: 4x4"
      rect3 = "#3 @ 5,5: 2x2"
      example1 = [rect1, rect2, rect3]
      example1real = map parseRect example1
      rect1real = example1real !! 0
      rect2real = example1real !! 1
      rect3real = example1real !! 2
  describe "day03a" $ do
    it "parseRect" $ do
      rect1real `shouldBe` Rect 1 1 3 4 4
      rect2real `shouldBe` Rect 2 3 1 4 4
      rect3real `shouldBe` Rect 3 5 5 2 2
    it "contains" $ do
      rect1real `contains` (1, 2) `shouldBe` False
      rect1real `contains` (1, 3) `shouldBe` True
      rect1real `contains` (5, 4) `shouldBe` False
      rect2real `contains` (5, 4) `shouldBe` True
      rect3real `contains` (5, 4) `shouldBe` False
    it "numContains" $ do
      numContains example1real (1, 3) `shouldBe` 1
      numContains example1real (3, 3) `shouldBe` 2
      numContains example1real (3, 4) `shouldBe` 2
      numContains example1real (4, 3) `shouldBe` 2
      numContains example1real (4, 4) `shouldBe` 2
      numContains example1real (5, 4) `shouldBe` 1
      numContains example1real (4, 3) `shouldBe` 2
    it "example1" $ do
      partA example1 `shouldBe` 4
  describe "day03b" $ do
    it "overlaps" $ do
      overlaps rect1real rect2real `shouldBe` True
      overlaps rect1real rect3real `shouldBe` False
      overlaps rect2real rect3real `shouldBe` False
    it "anyOverlaps" $ do
      anyOverlaps [rect1real, rect2real] rect3real `shouldBe` False
    it "example1" $ do
      partB example1 `shouldBe` 3
