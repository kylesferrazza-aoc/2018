module Day01Spec where

import Test.Hspec

import Day01

spec :: Spec
spec = do
  let example1 = [1, -2, 3, 1]
  describe "day01a" $ do
    it "example 1" $ do
      partA example1 `shouldBe` 3
    it "example 2" $ do
      partA [1, 1, 1] `shouldBe` 3
    it "example 3" $ do
      partA [1, 1, -2] `shouldBe` 0
    it "example 4" $ do
      partA [-1, -2, -3] `shouldBe` -6
  describe "day01b" $ do
    it "example 1" $ do
      partB example1 `shouldBe` 2
    it "example 2" $ do
      partB [1, -1] `shouldBe` 0
    it "example 3" $ do
      partB [3, 3, 4, -2, -4] `shouldBe` 10
    it "example 4" $ do
      partB [-6, 3, 8, 5, -6] `shouldBe` 5
    it "example 5" $ do
      partB [7, 7, -2, -7, -4] `shouldBe` 14
