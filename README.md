# adventofcode2018

[![Build Status](https://travis-ci.org/kylesferrazza/adventofcode2018.svg?branch=master)](https://travis-ci.org/kylesferrazza/adventofcode2018)

I am going to be learning Haskell by making myself use it for [Advent of Code 2018](https://adventofcode.com/2018).

Should be fun.

[Click here](https://kylesferrazza.com/adventofcode2018) to view the documentation.

Documentation can be regenerated with `stack haddock --haddock-arguments "-o docs"`
