-- |Solutions to Day 4 problems.
module Day04 where

import Lib

import Text.Regex(mkRegex, matchRegex)
import Data.List(sort)
import Text.Printf
import Data.Ord(comparing)
import Data.Maybe(catMaybes)

-- |Calculate the answer to Day 4 part A from the input file.
day04a :: IO ()
day04a = do
  fileLines <- readLines "in/4"
  print $ partA fileLines

-- |Find the guard that has the most minutes asleep. What minute does that guard spend asleep the most?
-- What is the ID of the guard you chose multiplied by the minute you chose?
partA :: [String] -> Int
partA strs =
  let mlog = map parseLogEntry strs :: [Maybe LogEntry]
      log = sort $ catMaybes mlog :: Log
      sleepyGuard = findSleepiestGuard log :: Int
      sleepyMinute = findSleepiestMinute log sleepyGuard :: Int
  in sleepyGuard * sleepyMinute

-- |Returns the ID of the guard from the log with the most minutes asleep.
findSleepiestGuard :: Log -> Int
findSleepiestGuard log = 0

-- |Returns the minute that the guard with the given id spends asleep the most.
findSleepiestMinute
  :: Log -- ^ The log to check.
  -> Int -- ^ The ID of the guard to check.
  -> Int -- ^ The number of the minute which that guard speeds asleep the most.
findSleepiestMinute log id = 0

-- |The log of all guard sleeping patterns.
type Log = [LogEntry]

-- |An entry from the log of guard's sleeping patterns.
data LogEntry = LogEntry {
  -- |The time at which this entry occured.
  time :: Time,

  -- |The contents of this entry.
  contents :: EntryContents
} deriving (Show, Eq, Ord)

-- |Any entry can represent a guard beginning a shift, falling asleep, or waking up.
data EntryContents =
  -- |A guard has begun their shift.
  Begin {
    -- |The ID of the guard who just began their shift.
    guardId :: Int
  } |
  -- |A guard has fallen asleep.
  Asleep |
  -- |A guard has woken up.
  Awake
  deriving (Show, Eq, Ord)

-- |A timestamp, to be associated with each 'LogEntry'.
data Time = Time {
  -- |The associated year.
  year :: Int,

  -- |The associated month.
  month :: Int,

  -- |The associated day.
  day :: Int,

  -- |The associated hour.
  hour :: Int,

  -- |The associated minute.
  minute :: Int
} deriving (Show, Eq, Ord)

-- |Parse log entry from a string.
parseLogEntry :: String -> Maybe LogEntry
parseLogEntry str =
  let startPattern = mkRegex "^\\[([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+)\\] (.*)$"
      beginPattern = mkRegex "^Guard #([0-9]+) begins shift$"
      asleepPattern = mkRegex "^falls asleep$"
      wakePattern = mkRegex "^wakes up$"
      matches = matchRegex startPattern str :: Maybe [String]
      in case matches of
        Nothing -> Nothing
        Just attrs ->
          let [_, _, _, _, _, rest] = attrs
              [y, mon, d, h, min, _] = map read attrs
              t = Time y mon d h min
              matchedBegin = matchRegex beginPattern rest
              in case matchedBegin of
                Just [id] -> Just $ LogEntry t $ Begin $ read id
                Nothing -> let matchedAsleep = matchRegex asleepPattern rest
                  in case matchedAsleep of
                    Just _ -> Just $ LogEntry t Asleep
                    Nothing -> let matchedAwake = matchRegex wakePattern rest
                      in case matchedAwake of
                        Just _ -> Just $ LogEntry t Awake
                        Nothing -> Nothing
