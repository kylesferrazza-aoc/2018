-- |Common functions for all modules.
module Lib where

-- |Given a path to a file, reads it as an array of strings.
readLines :: FilePath -> IO [String]
readLines = fmap lines . readFile

-- |Removes a leading "+" from the given string, if there is one.
removeLeadingPlus :: String -> String
removeLeadingPlus ('+':rst) = rst
removeLeadingPlus str = str

-- |Given a path to a file, reads it as an array of ints (one per line).
readInts :: FilePath -> IO [Int]
readInts p = do
  strs <- readLines p
  return $ map (read . removeLeadingPlus) $ strs
