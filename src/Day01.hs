-- |Solutions to Day 1 problems.
module Day01 where

import Lib

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet

-- |Calculate the answer to Day 1 part A from the input file.
day01a :: IO ()
day01a = do
  ints <- readInts "in/1"
  print $ partA $ ints

-- |Starting with a frequency of zero, what is the resulting frequency after all of the changes in frequency have been applied?
partA :: [Int] -> Int
partA = sum

-- |Calculate the answer to Day 1 part B from the input file.
day01b :: IO ()
day01b = do
  ints <- readInts "in/1"
  print $ partB ints

-- |What is the first frequency your device reaches twice?
partB :: [Int] -> Int
partB lst = partBacc (cycle lst) 0 IntSet.empty

-- |Like 'partB' but with an accumulator for the current frequency.
partBacc :: [Int] -> Int -> IntSet -> Int
partBacc [] _ _ = error "no frequencies left in list"
partBacc (x:xs) curFreq set =
  if IntSet.member curFreq set
    then curFreq
    else
      let newFreq = curFreq + x
          newSet = IntSet.insert curFreq set
      in partBacc xs newFreq newSet
