-- |Solutions to Day 3 problems.
module Day03 where

import Lib

import Text.Regex.Posix
import Data.Maybe(fromJust)
import Data.List(delete, elemIndex)

-- |Calculate the answer to Day 3 part A from the input file.
day03a :: IO ()
day03a = do
  fileLines <- readLines "in/3"
  print $ partA fileLines

-- |How many square inches of fabric are within two or more claims?
partA :: [String] -> Int
partA strs =
  let rects = map parseRect strs
      pts = [(x, y) | x <- [0..1000], y <- [0..1000]]
      mapped = map (numContains rects) pts
      filtered = filter (>=2) mapped
  in length filtered

-- |A rectangle.
data Rect = Rect {
  -- |The ID of this rectangle.
  rid :: Int,

  -- |The distance from the left of the grid to the start of this rectangle.
  fromLeft :: Int,

  -- |The distance from the top of the grid to the start of this rectangle.
  fromTop :: Int,

  -- |The width of this rectangle.
  width :: Int,

  -- |The height of this rectangle.
  height :: Int
} deriving (Show, Eq)

-- |How many of the given rects contain the point?
numContains :: [Rect] -> (Int, Int) -> Int
numContains r p =
  let containsP = flip contains p
  in length $ filter containsP r

-- |Does the rect contain the point given by (x, y)?
contains :: Rect -> (Int, Int) -> Bool
contains (Rect _ l t w h) (x, y) =
  x >= l && x < l+w && y >= t && y < t+h

-- |Creates a rectangle from the given input string.
parseRect :: String -> Rect
parseRect str =
  let pattern = "^#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)$"
      (_, _, _, attrs) = str =~ pattern :: (String, String, String, [String])
      [i, l, t, w, h] = map read attrs
  in Rect i l t w h

-- |Calculate the answer to Day 3 part B from the input file.
day03b :: IO ()
day03b = do
  fileLines <- readLines "in/3"
  print $ partB fileLines

-- |What is the ID of the only claim that doesn't overlap?
partB :: [String] -> Int
partB strs =
  let rects = map parseRect strs
      mapped = map f rects where
        f :: Rect -> Bool
        f rect = let excepts = delete rect rects
          in anyOverlaps excepts rect
      ind = fromJust $ elemIndex False mapped
      theRect = rects !! ind
  in rid theRect

-- |Returns whether the given rectangle overlaps with any rect from the array.
anyOverlaps :: [Rect] -> Rect -> Bool
anyOverlaps rects rect =
  any (overlaps rect) rects

-- |Returns whether r1 and r2 overlap.
overlaps :: Rect -> Rect -> Bool
overlaps (Rect _ l1 t1 w1 h1) (Rect _ l2 t2 w2 h2) =
  let oneLeft  = (l1 > l2 + w2-1) || (l2 > l1 + w1-1)
      oneAbove = (t1 > t2 + h2-1) || (t2 > t1 + h1-1)
  in (not oneLeft) && (not oneAbove)
