-- |Solutions to Day 2 problems.
module Day02 where

import Lib

import qualified Data.Map.Strict as Map

-- |Calculate the answer to Day 2 part A from the input file.
day02a :: IO ()
day02a = do
  fileLines <- readLines "in/2"
  print $ partA fileLines

-- |What is the checksum for your list of box IDs?
partA :: [String] -> Int
partA strs =
  let twos = length $ filter (/=0) $ map countTwos strs
      threes = length $ filter (/=0) $ map countThrees strs
  in twos * threes

-- |Count the number of times any letter appears n times in str.
countNs :: Int -> [Char] -> Int
countNs n str = countNsAcc str Map.empty n

-- |Like 'countNs', but with a Map to accumulate letter frequencies.
countNsAcc :: [Char] -> Map.Map Char Int -> Int -> Int
countNsAcc (c:cs) theMap n =
  let newMap = Map.insertWith (+) c 1 theMap
  in countNsAcc cs newMap n
countNsAcc [] theMap n =
  length $ Map.filter (==n) theMap

-- |Count the number of times any letter appears twice in the given string.
countTwos :: [Char] -> Int
countTwos = countNs 2

-- |Count the number of times any letter appears three times in the given string.
countThrees :: [Char] -> Int
countThrees = countNs 3

-- |Calculate the answer to Day 2 part B from the input file.
day02b :: IO ()
day02b = do
  fileLines <- readLines "in/2"
  putStrLn $ partB fileLines

-- |What letters are common between the two correct box IDs?
partB :: [String] -> String
partB strs =
  let (a, b) = findBest strs
  in common a b

-- |Returns the strings that are 1 substitution apart in the given list.
findBest :: [String] -> (String, String)
findBest strs = findBestAcc strs strs

-- |Like 'findBest', but with an accumulator for strs that are left.
findBestAcc :: [String] -> [String] -> (String, String)
findBestAcc [] _ = error "no best"
findBestAcc (s:ss) strs =
  let filtered = filter (\str -> difference str s == 1) strs
      len = length filtered
  in if len == 1
     then (filtered !! 0, s)
     else findBestAcc ss strs

-- |Returns the number of characters shared by the given strings.
-- Errors if the given strings are not the same length.
difference :: String -> String -> Int
difference [] (_:_) = error "strings not the same length"
difference (_:_) [] = error "strings not the same length"
difference [] [] = 0
difference (a:as) (b:bs) =
  let add = if a /= b then 1 else 0
  in add + difference as bs

-- |Returns the string of all characters common between the given strings.
-- Errors if the given strings are not the same length.
common :: String -> String -> String
common [] (_:_) = error "strings not the same length"
common (_:_) [] = error "strings not the same length"
common [] [] = ""
common (a:as) (b:bs) =
  let rest = common as bs
  in if a == b
     then a:rest
     else rest
